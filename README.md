# **moviesInfo** #

## **API** ##

### Movies list ###
GET http://localhost:8080/samplerest/rest/movies  

### Movie preview using movie_id ###
GET http://localhost:8080/samplerest/rest/movies/{movie_id}

### New movie adding ###
POST http://localhost:8080/samplerest/rest/movies  
XML example:
```
<movie>
    <title>The great American cross-country road race.</title>
    <description>It's about love.</description>
    <mark>9,0</mark>
</movie>
```
### Description update ###
PUT http://localhost:8080/samplerest/rest/movies/{movie_id}/description   
XML example:

```
<movie>
    <description>It's about trust</description>
</movie>

```
### Displaying comments to movie ###
GET http://localhost:8080/samplerest/rest/movies/{movie_id}/comments

### Adding comment to movie ###
POST http://localhost:8080/samplerest/rest/movies/{movie_id}/comments

### Comment deleting ###
DELETE http://localhost:8080/samplerest/rest/movies/{movie_id}/comments/{comment_id}

### Actor adding ###
POST http://localhost:8080/samplerest/rest/actors  
XML example:
```
<actor>
    <firstname>John</firstname>
    <lastname>Thebest</lastname>
</actor>
```

### Adding movie to actor ###
POST http://localhost:8080/samplerest/rest/actors/{actor_id}/movies  
XML example:

```
<movie>
    <title>The one and only.</title>
    <description>Lack of action</description>
    <mark>8,2</mark>
</movie>
```

### Adding actor to movie ###
POST http://localhost:8080/samplerest/rest/movie/{movie_id}/actors  
XML example:

```
<actor>
    <firstname>Johann</firstname>
    <lastname>Kreuzung</lastname>
</actor>
```