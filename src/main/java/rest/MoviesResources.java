package rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import domain.Actor;
import domain.Comment;
import domain.Movie;

@Path("/movies")
@Stateless
public class MoviesResources {
	
	@PersistenceContext
	EntityManager em;
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public List<Movie> getAll()
	{
		return em.createNamedQuery("movie.all", Movie.class).getResultList();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Response get(@PathParam("id") int id){
		Movie result = em.createNamedQuery("movie.id", Movie.class)
				.setParameter("movieId", id)
				.getSingleResult();
		if(result==null){
			return Response.status(404).build();
		}
		return Response.ok(result).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	public Response Add(Movie movie){
		em.persist(movie);
		return Response.ok(movie.getId()).build();
	}
	
	@PUT
	@Path("/{id}/description")
	@Consumes(MediaType.APPLICATION_XML)
	public Response update(@PathParam("id") int id, Movie m){
		Movie result = em.createNamedQuery("movie.id", Movie.class)
				.setParameter("movieId", id)
				.getSingleResult();
		if(result==null)
			return Response.status(404).build();
		result.setDescription(m.getDescription());
		em.persist(result);
		return Response.ok().build();
	}
	
	@GET
	@Path("/{movieId}/comments")
	@Produces(MediaType.APPLICATION_XML)
	public List<Comment> getComments(@PathParam("movieId") int movieId){
		Movie result = em.createNamedQuery("movie.id", Movie.class)
				.setParameter("movieId", movieId)
				.getSingleResult();
		if(result==null)
			return null;
		return result.getComments();
	}
	
	@POST
	@Path("/{id}/comments")
	@Consumes(MediaType.APPLICATION_XML)
	public Response addComment(@PathParam("id") int movieId, Comment comment){
		Movie result = em.createNamedQuery("movie.id", Movie.class)
				.setParameter("movieId", movieId)
				.getSingleResult();
		if(result==null)
			return Response.status(404).build();
		result.getComments().add(comment);
		comment.setMovie(result);
		em.persist(comment);
		return Response.ok().build();
	}
	
	@DELETE
    @Path("/{id}/comments/{commentId}")
    public Response deleteComment(@PathParam("id") int id,
    		@PathParam("commentId") int commentId) {
        Movie result = em.createNamedQuery("movie.id", Movie.class)
                    .setParameter("movieId", id)
                    .getSingleResult();
        if(result==null)
			return Response.status(404).build();
        List<Comment> comments = result.getComments();
        for (int i = 0; i < comments.size(); i++) {
            if (comments.get(i).getId() == (commentId)) {
                comments.get(i).setMovie(null);
                comments.remove(i);
            }
        }
        em.merge(result);

        return Response.ok().build();
}
	
	@GET
	@Path("/{movieId}/actors")
	@Produces(MediaType.APPLICATION_XML)
	public List<Actor> getActors(@PathParam("movieId") int movieId){
		Movie result = em.createNamedQuery("movie.id", Movie.class)
				.setParameter("movieId", movieId)
				.getSingleResult();
		if(result==null)
			return null;
		return result.getActors();
	}
}
