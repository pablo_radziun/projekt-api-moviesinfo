package rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import domain.Actor;
import domain.Movie;

@Path("/actors")
@Stateless
public class ActorsResources {
	
	@PersistenceContext
	EntityManager em;
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	public Response Add(Actor actor){
		em.persist(actor);
		return Response.ok(actor.getId()).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public List<Actor> getAll()
	{
		return em.createNamedQuery("actor.all", Actor.class).getResultList();
	}
	
	
	@GET
	@Path("/{actorId}/movies")
	@Produces(MediaType.APPLICATION_XML)
	public List<Movie> getMovies(@PathParam("actorId") int actorId){
		Actor result = em.createNamedQuery("actor.id", Actor.class)
				.setParameter("actorId", actorId)
				.getSingleResult();
		if(result==null)
			return null;
		return result.getMovies();
	}
	
	@POST
	@Path("/{id}/movies")
	@Consumes(MediaType.APPLICATION_XML)
	public Response addMovie(@PathParam("id") int actorId, Movie movie){
		Actor result = em.createNamedQuery("actor.id", Actor.class)
				.setParameter("actorId", actorId)
				.getSingleResult();
		if(result==null)
			return Response.status(404).build();
		result.getMovies().add(movie);
		movie.setActor(result);
		em.persist(movie);
		return Response.ok().build();
	}

}
